Soal 1 Membuat Database :
	CREATE DATABASE myshop;

Soal 2 Membuat Table di Dalam Database :
	CREATE TABLE `users`(
		`id` int(10) PRIMARY KEY AUTO_INCREMENT,
	    	`name` varchar(255) NOT NULL,
	    	`email` varchar(255) NOT NULL,
	    	`password` varchar(255) NOT Null
	);

	CREATE TABLE `categories`(
		`id` int(10) PRIMARY KEY AUTO_INCREMENT,
	    	`name` varchar(255) NOT NULL
	);

	CREATE TABLE `items`(
		`id` int(10) PRIMARY KEY AUTO_INCREMENT,
	    	`name` varchar(255) NOT NULL,
	    	`description` varchar(255) NOT NULL,
		`price` int(20) NOT NULL,
	    	`stock` int(20) NOT NULL,
	    	`category_id` int(10) NOT NULL,
	    	FOREIGN KEY (`category_id`) REFERENCES `categories`(`id`)
	);

Soal 3 Memasukkan Data pada Table :
	INSERT INTO `users` (`name`,`email`,`password`) VALUES
	('John Doe','john@doe.com','john123'),
	('Jane Doe','jane@doe.com','jenita123');

	INSERT INTO `categories`(`name`) VALUES
	('gadget'),('cloth'),('men'),('women'),('branded');

	INSERT INTO `items`(`name`,`description`,`price`,`stock`,`category_id`) VALUES
	('Sumsang b50','hape keren dari merek sumsang','4000000','100','1'),
	('Uniklooh','baju keren dari brand ternama','500000','50','2'),
	('IMHO Watch','jam tangan anak yang jujur banget','2000000','10','1');

Soal 4 Mengambil Data dari Database :
a. Mengambil data users :
	SELECT `id` AS 'ID', `name` AS 'Nama', `email` AS 'Email' FROM users;

b. Mengambil data items :
-Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
	SELECT * FROM `items` WHERE `price`>1000000;

-Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
	SELECT * FROM `items` WHERE `name` LIKE '%sang%';

c. Menampilkan data items join dengan kategori
	SELECT `items`.`name`,`items`.`description`,`items`.`price`,`items`.`stock`,`items`.`category_id`,`categories`.`name` AS 'kategori' FROM `items` INNER JOIN `categories` ON `categories`.`id`=`items`.`category_id`;

Soal 5 Mengubah Data dari Database :
	UPDATE `items` SET `price`=2500000 WHERE `name`='sumsang b50';